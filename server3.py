import zmq
import json
import time

class Dispatcher:
    def __init__(self, serv_port=10000):
        context = zmq.Context()
        self.incoming_socket = context.socket(zmq.REP)
        self.incoming_socket.bind ("tcp://*:%s" % serv_port)

        self.cmd_socket = context.socket(zmq.PUB)
        self.cmd_socket.bind("tcp://*:%s" % (serv_port+1))

        
    def start(self):

        while True:
            string = self.incoming_socket.recv()
            cmd, arguments, messagedata = string.split(maxsplit=2)
            self.incoming_socket.send(b'OK')

            self.cmd_socket.send(string)

                



test=Dispatcher()
test.start()


