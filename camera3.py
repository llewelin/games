#!/usr/bin/python3

import platform
import time
import zmq
import multiprocessing
import json

class Camera:
    def __init__(self, server_ip = 'localhost', server_port=10000, name=None, idx=0):
        if name==None:
            name = platform.node()
        self.name = name
        self.idx = idx

        self.data = multiprocessing.Manager().dict()
        self.data['freq'] = 1
        self.data['resolution'] = (800, 600)

        self.context = zmq.Context()
        self.video_socket = self.context.socket(zmq.REQ)
        self.video_socket.connect("tcp://%s:%s" % (server_ip, server_port))

        # self.cmd_socket = self.context.socket(zmq.SUB)
        # self.cmd_socket.connect("tcp://%s:%s" % (server_ip, server_port+1))
        # self.cmd_socket.setsockopt_string(zmq.SUBSCRIBE, 'set_freq')


        self.server_port = server_port
        self.server_ip = server_ip

        
        multiprocessing.Process(target=self.listen_orders).start()

    def listen_orders(self):
        context = zmq.Context()

        cmd_socket = context.socket(zmq.SUB)
        cmd_socket.connect("tcp://%s:%s" % (self.server_ip, self.server_port+1))
        cmd_socket.setsockopt_string(zmq.SUBSCRIBE, self.name)

        while True:
            a = cmd_socket.recv().split(maxsplit=2)
            cmd, args= a[1], a[2]

            if cmd == b'set_freq':
                print('Change Freq', float(args))
                self.data['freq'] = float(args)
        
    def send_video(self, data):
        messagedata = 'raw %s %s' % (self.name, json.dumps(data))
        self.video_socket.send_string(messagedata)
        message = self.video_socket.recv()
        print(message)
            
    def start(self):
        while True:
            self.send_video(self.data['resolution'])
            time.sleep(1/self.data['freq'])

test=Camera()
test.start()

