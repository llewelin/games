import time
from imutils.video import VideoStream
import imagezmq
import sys

sender = imagezmq.ImageSender(connect_to='tcp://localhost:5555')

label = sys.argv[1]

camera = VideoStream(src=int(sys.argv[2])).start()
time.sleep(2.0)  # allow camera sensor to warm up
while True:  # send images as stream until Ctrl-C
    image = camera.read()
    sender.send_image(label, image)
    time.sleep(.1)

