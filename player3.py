import zmq

class Player:
    def __init__(self, server_ip='localhost', server_port = 10000):
        self.server_ip = server_ip
        self.server_port = server_port

    def start_receiving_camera(self):
        context = zmq.Context()

        cmd_socket = context.socket(zmq.SUB)
        cmd_socket.connect("tcp://%s:%s" % (self.server_ip, self.server_port+1))
        cmd_socket.setsockopt_string(zmq.SUBSCRIBE, 'raw')

        while True:
            print('waiting')
            data = cmd_socket.recv().split(maxsplit=2)
            cmd, args= data[1], data[2]
            print(cmd, args)


    def send_freq_change(self, camera, new_freq):
        context = zmq.Context()
        cmd_socket = context.socket(zmq.REQ)
        cmd_socket.connect("tcp://%s:%s" % (self.server_ip, self.server_port))
        cmd_socket.send(b'%s set_freq %s' % (camera, str(new_freq).encode()))

            
test = Player()
#test.start_receiving_camera()
test.send_freq_change(b'ganelon', .2)
