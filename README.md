# Code from

https://github.com/jeffbass/imagezmq

https://www.pyimagesearch.com/2019/04/15/live-video-streaming-over-network-with-opencv-and-imagezmq/

Potentiel graphical interface

https://kivy.org/#home

# Mini tutorial

Start in one terminal 

python3 ./host.py

Start in an other terminal

python3 ./client.py "Camera One" 0

And in an other one

python3 ./client.py "Camera Two" 2
